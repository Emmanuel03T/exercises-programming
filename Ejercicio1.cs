using System;

namespace Ejercicio1.Programa_de_Numeros_Positivos
{
    class Program
    {
        static void Main(string[] args)
            
        {
            int numeros, cont = 0;

            
 
            do
            {
                Console.WriteLine("Introducir Números Positivos");
                numeros = Convert.ToInt32(Console.ReadLine());

                if (numeros > 0)
                {
                    Console.WriteLine("Número Aceptado....");
                    Console.WriteLine();
                    cont = numeros + cont;
                }
                if (numeros < -1)
                {
                    Console.WriteLine("Debe Insertar un Número Positivo.");
                    Console.WriteLine();
                }
                

            } while (numeros != 0);

            Console.WriteLine("La Suma de todos los Números Insertados es: " + cont);
        }
    }
}
