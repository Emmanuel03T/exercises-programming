﻿using System;

namespace Ejercicio2_Crear_un_programa_para_escribir_del_1_10
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;
            Console.WriteLine("Los Números del 1 al 10 son: ");

            do
            {
                num = num + 1;
                Console.WriteLine(num);
            } while (num < 10);
        }
    }
}
