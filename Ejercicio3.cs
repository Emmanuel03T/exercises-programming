﻿using System;

namespace Ejercicio3_Primeross_10_numeros_pares_a_partir_del_producto_de_10x10
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 100;
            Console.WriteLine("Los Primeros Números Pares a Partir del Producto de 10x10 son: ");
            do
            {
                num = num + 2;
                Console.WriteLine(num);

            } while (num <= 118);
        }
    }
}
