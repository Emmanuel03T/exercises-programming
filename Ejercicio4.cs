﻿using System;

namespace Ejercicio4
{
    class Program
    {
        static void Main(string[] args)
        {
            char letra = 'Z';

            while (letra >= 'A')
            {
                Console.Write("-" + letra);

                letra--;

            }

            Console.ReadLine();
        }
    }
}
